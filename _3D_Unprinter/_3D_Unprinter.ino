/* _3D_Unprinter - Software for Arduino Uno to operate Antwheight Robot
*  Maybe you worry about how long digitalRead() takes? 
*  I think it is about 3.7us, so the program will read the input ~270 per 1ms, which is the shortest pulse.
*
*  Currently this program is relient on the global variables, but oh well
*  Also, every 70mins the timer will overflow and you will get a glitch!
* 
*   Sam Walder - University of Bristol - 2015 - sam.walder@bristol.ac.uk
*
*   -- CHANGE LOG --
*   * 2015-11-13 - Created
*/

// Define what pins are used for wich servo signals
#define S1 A0
#define S2 A1
#define S3 A2
#define S4 A3
#define S5 6
#define S6 7

// Define which pins are used for which motor thing
#define mA_spd 3
#define mA_dir 2
#define mB_spd 5
#define mB_dir 4
#define mC_spd 6
#define mC_dir 7

// Initiate all the variables
  // Timer variables
  int time_start;
  int time_s1;
  int time_s2;
  int time_s3;
  int time_s4;
  int time_s5;
  int time_s6;
  
  //Time differences
  int diff_s1;
  int diff_s2;
  int diff_s3;
  int diff_s4;
  int diff_s5;
  int diff_s6;
  
  // Variables for driving
  int dir_R;  // Amount of R direction demand
  int dir_I;  // Amount of I direction demand
  int m_A;    // Amount of motor A demand
  int m_B;    // Amount of motor B demand
  int m_C;    // Amount of motor C demand
  int m_spin; // Amount of spin demand
  
  // Stuff for the slider debugger
  int a;
  int b;
  int mark;

void setup() 
{ 
  // Initiate the pins for reading in the servo signals
  pinMode(S1, INPUT);    // Servo signal 1
  pinMode(S2, INPUT);    // Servo signal 2
  pinMode(S3, INPUT);    // Servo signal 3
  pinMode(S4, INPUT);    // Servo signal 4
  pinMode(S5, INPUT);    // Servo signal 5
  pinMode(S6, INPUT);    // Servo signal 6
  
  // Initiate the pins for the motor outputs
  pinMode(mA_spd, OUTPUT);
  pinMode(mA_dir, OUTPUT);
  pinMode(mB_spd, OUTPUT);
  pinMode(mB_dir, OUTPUT);
  pinMode(mC_spd, OUTPUT);
  pinMode(mC_dir, OUTPUT);
  
  // Initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
} 
 
void loop() 
{ 
  Serial.println("Start");
  
  // Get all the servo values
  read6PPM();
  
  // Calculate the diffs
  diff_s1 = time_s1 - time_start;
  diff_s2 = time_s2 - time_s1;
  diff_s3 = time_s3 - time_s2;
  diff_s4 = time_s4 - time_s3;
  diff_s5 = time_s5 - time_s4;
  diff_s6 = time_s6 - time_s5;
  
  // Calculate R and I in the range -100<x<100
  m_spin = (diff_s4 - 1500)/5;;
  dir_R  = (diff_s1 - 1500)/5;
  dir_I  = (diff_s2 - 1500)/5;
  
  // Calculate motor demands
  m_A = (-dir_R/2)     + m_spin;
  m_B = (dir_R-dir_I)  + m_spin;
  m_C = (dir_R+dir_I)  + m_spin;
  
  // Set the motor pins accordingly
  setMotorA(m_A);
  setMotorB(m_B);
  setMotorC(m_C);
  
  /*
  // Print this to the PC
  Serial.print("dir_R = ");
  Serial.println(dir_R);
  Serial.print("dir_I = ");
  Serial.println(dir_I);
  
  Serial.print("m_A = ");
  Serial.println(m_A);
  Serial.print("m_B = ");
  Serial.println(m_B);
  Serial.print("m_C = ");
  Serial.println(m_C);

  Serial.println();
  */
  
  // Print this to the PC for funs
  /*
   // Clear the screen
  for(a=1; a<16; a++){
    Serial.println();
  }
  
  Serial.print("CH1 - ");
  mark = (round((diff_s1-1000) / 100)) * 2;
  for(a=1; a<mark; a++){
    Serial.print("#");
  }
  for(b=mark; b<20; b++){
    Serial.print("-");
  }
  Serial.println("");
  
  Serial.print("CH2 - ");
  mark = (round((diff_s2-1000) / 100)) * 2;
  for(a=1; a<mark; a++){
    Serial.print("#");
  }
  for(b=mark; b<20; b++){
    Serial.print("-");
  }
  Serial.println("");
  
  Serial.print("CH3 - ");
  mark = (round((diff_s3-1000) / 100)) * 2;
  for(a=1; a<mark; a++){
    Serial.print("#");
  }
  for(b=mark; b<20; b++){
    Serial.print("-");
  }
  Serial.println("");
  
  Serial.print("CH4 - ");
  mark = (round((diff_s4-1000) / 100)) * 2;
  for(a=1; a<mark; a++){
    Serial.print("#");
  }
  for(b=mark; b<20; b++){
    Serial.print("-");
  }
  Serial.println("");
  */
  /*
  Serial.print("CH5 - ");
  mark = (round((diff_s5-1000) / 100)) * 2;
  for(a=1; a<mark; a++){
    Serial.print("#");
  }
  for(b=mark; b<20; b++){
    Serial.print("-");
  }
  Serial.println("");
  
  Serial.print("CH6 - ");
  mark = (round((diff_s6-1000) / 100)) * 2;
  for(a=1; a<mark; a++){
    Serial.print("#");
  }
  for(b=mark; b<20; b++){
    Serial.print("-");
  }
  Serial.println("");
  */
  

} 

void read6PPM(){
  // We wait for the first servo signal to go high then start timeing
  // Following this we record the time each siganl goes low#
  
  // Wait for the first servo signal to go high
  while(digitalRead(S1) == LOW){
    // Do nothing, wait, act all nonchalant
  }
  // It is high so record the timer value!
  time_start = micros();
  
  // Wait for the 2nd servo signal to go high
  while(digitalRead(S2) == LOW){
    // Do nothing, wait, act all nonchalant
  }  
  // It is high so record the timer value!
  time_s1 = micros();
  
  // Wait for the 3rd servo signal to go high
  while(digitalRead(S3) == LOW){
    // Do nothing, wait, act all nonchalant
  }  
  // It is high so record the timer value!
  time_s2 = micros();
  
  // Wait for the 4th servo signal to go high
  while(digitalRead(S4) == LOW){
    // Do nothing, wait, act all nonchalant
  }  
  // It is high so record the timer value!
  time_s3 = micros();
  
  /*
  // Wait for the 5th servo signal to go high
  while(digitalRead(S5) == 'LOW'){
    // Do nothing, wait, act all nonchalant
  }  
  // It is high so record the timer value!
  time_s4 = micros();
  
  // Wait for the 6th servo signal to go high
  while(digitalRead(S6) == 'LOW'){
    // Do nothing, wait, act all nonchalant
  }  
  // It is high so record the timer value!
  time_s5 = micros();
  */
  
  // Wait for the 4th servo signal to go low
  while(digitalRead(S4) == HIGH){
    // Do nothing, wait, act all nonchalant
  }  
  // It is high so record the timer value!
  time_s4 = micros();
}

void setMotorA(int demand) {
  // sets the PWM and direction outputs for motor A based on the demand
  int scaled_demand; // somewher to store the scaled demand
  constrain(demand, -100, 100);
  scaled_demand = map(abs(demand), 0, 100, 40, 255);  
  analogWrite(mA_spd, round(scaled_demand) );
  if(demand<0){
    digitalWrite(mA_dir, LOW);
  }
  else {
    digitalWrite(mA_dir, HIGH);
  }
}

void setMotorB(int demand) {
  // sets the PWM and direction outputs for motor B based on the demand
  int scaled_demand; // somewher to store the scaled demand
  constrain(demand, -100, 100);
  scaled_demand = map(abs(demand), 0, 100, 40, 255);  
  analogWrite(mB_spd, round(scaled_demand) );
  if(demand<0){
    digitalWrite(mB_dir, LOW);
  }
  else {
    digitalWrite(mB_dir, HIGH);
  }
}

void setMotorC(int demand) {
  // sets the PWM and direction outputs for motor A based on the demand
  int scaled_demand; // somewher to store the scaled demand
  constrain(demand, -100, 100);
  scaled_demand = map(abs(demand), 0, 100, 40, 255);  
  analogWrite(mC_spd, round(scaled_demand) );
  if(demand<0){
    digitalWrite(mC_dir, LOW);
  }
  else {
    digitalWrite(mC_dir, HIGH);
  }
}




